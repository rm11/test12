#!/bin/sh

export
ssh $1 /bin/bash -c " \
    docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com \
    && docker pull registry.gitlab.com/tailored-labs/emotive-client \
    && export CONTAINER_ID=$(docker container ls --filter label=emotive-client -q) \
    && docker stop $CONTAINER_ID \
    && docker rm $CONTAINER_ID \
    && docker run -p 3000:80 -e API_URL=https://staging.api.com -t registry.gitlab.com/tailored-labs/emotive-client"
